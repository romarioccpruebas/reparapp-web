$(document).ready(function(){
    $('#submitLogin').click(function(event) {
        event.preventDefault();
        ajaxLogin('/login');
    });
});
function ajaxLogin (url) {
    var dataSend = $('#login-form').serialize();
    //var alerta = $('#message_alert');
    $.post( url, dataSend, function( data ) {
        if(data.status == 'ok'){
            alerta.css('display','none');
            alerta.text('');
            window.location.href = "/";
        } else if (data.status == 'activar') {
            window.location.href = "activar-cuenta";
        } else if (data.status == 'error') {
            var message = data.messages;
            alerta.addClass('alert alert-danger');
            alerta.text(message);
        } else if (data.status == 'disable') {
            var message = data.messages;
            alerta.addClass('alert alert-danger');
            alerta.text(message);
        }
        else {
            window.location.href = "/";
        }
    }, "json");
}