<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2012 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application;

use Application\Form\LoginSesionForm;
use Application\Interfaces\AdminInterface;
use Application\Interfaces\IndexInterface;
use Zend\Authentication\Adapter\DbTable;
use Zend\Authentication\AuthenticationService;
use Zend\Mvc\ModuleRouteListener;
use Core\Model\User;
use Zend\Session\Config\SessionConfig;
use Zend\Session\Container;
use Zend\Session\SessionManager;

class Module
{
    public function getServiceConfig()
    {
        return [
            'factories' => [
                'Zend\Session\SessionManager' => function ($sm) {
                    $config = $sm->get('config');
                    if (isset($config['session'])) {
                        $session = $config['session'];

                        $sessionConfig = null;
                        if (isset($session['config'])) {
                            $options = isset($session['config']['options']) ? $session['config']['options'] : array();
                            $sessionConfig = new SessionConfig();
                            $sessionConfig->setOptions($options);
                        }

                        $sessionManager = new SessionManager();

                    } else {
                        $sessionManager = new SessionManager();
                    }

                    Container::setDefaultManager($sessionManager);
                    return $sessionManager;
                },

                'Core\Model\ReparappStorage' => function($sm){
                    return new \Core\Model\ReparappStorage('login_storage');
                },

                'AuthService' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $dbTableAuthAdapter = new DbTable($dbAdapter, 'users', 'email', 'password', 'MD5(?)');
                    $authService = new AuthenticationService();
                    $authService->setAdapter($dbTableAuthAdapter);
                    $authService->setStorage($sm->get('Core\Model\ReparappStorage'));
                    return $authService;
                },


            ],
            'invokables' => array(
                // defining it as invokable here, any factory will do too
                'my_image_service' => 'Imagine\Gd\Imagine',
            ),

        ];

    }

    public function onBootstrap($e)
    {
        $e->getApplication()->getServiceManager()->get('translator');
        $eventManager        = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);

        $form = new LoginSesionForm();

        $form->get('submit')->setValue('Login');

        $application = $e->getParam('application');
        $viewModel = $application->getMvcEvent()->getViewModel();
        $viewModel->form = $form;
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function getControllerConfig()
    {
        return array(
            'initializers' => array(
                function ($instance, $sm) {
                    $locator = $sm->getServiceLocator();

                    if ($instance instanceof IndexInterface) {
                        //$instance->setForm('CapexForm',new CapexForm($locator));
                        $instance->setModel('User',new User($locator));
                    }
                    if ($instance instanceof AdminInterface) {
                        $instance->setModel('User',new User($locator));
                    }
                },
            ),
        );
    }
}
