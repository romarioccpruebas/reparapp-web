<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2012 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Application\Interfaces\IndexInterface;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class IndexController extends AbstractActionController implements IndexInterface
{
    public function indexAction()
    {
        return new ViewModel();
    }

    public function setService($name,$service)
    {
        $this->{$name} = $service;
    }

    public function setForm($name,$form)
    {
        $this->{$name} = $form;
    }

    public function setModel($name, $model)
    {
        $this->{$name} = $model;
    }
}
