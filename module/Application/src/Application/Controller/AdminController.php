<?php
/**
 * Created by PhpStorm.
 * User: Carlos Ramos
 * Date: 12/05/17
 * Time: 12:27 AM
 */

namespace Application\Controller;

use Application\Interfaces\AdminInterface;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class AdminController extends AbstractActionController implements AdminInterface
{
    public function indexAction()
    {
        $users=$this->User->getUsers();

        return new ViewModel(array('usuarios'=>$users));
    }

    public function setService($name,$service)
    {
        $this->{$name} = $service;
    }

    public function setForm($name,$form)
    {
        $this->{$name} = $form;
    }

    public function setModel($name, $model)
    {
        $this->{$name} = $model;
    }
}
