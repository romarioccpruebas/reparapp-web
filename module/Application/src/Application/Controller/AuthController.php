<?php

namespace Application\Controller;

use Application\Form\LoginSesionForm;
use Application\Form\LoginValidator;
use Core\Model\Authentication;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class AuthController extends AbstractActionController
{

    protected $storage;
    protected $authservice;

    public function loginAction()
    {
        //setcookie("cambiaCuenta");
        $auth = $this->getAuthService();
        if ($auth->hasIdentity()) {

            //$data_sesion_user = $auth->getStorage()->read();
            $result = array(
                'status' => 'ok',
                'messages' => ''
            );
            $response = $this->getResponse();
            $response->getHeaders()->addHeaderLine('Content-Type', 'application/json');
            $response->setContent(json_encode($result));

            return $this->redirect()->toRoute('dashboard');
        }

        $request = $this->getRequest();
        $messages = null;
        if ($request->isPost()) {
            $formData = $request->getPost();
            $form_sesion = new LoginSesionForm();
            $formValidator = new LoginValidator();
            $form_sesion->setInputFilter($formValidator->getInputFilter());
            $form_sesion->setData($formData);
            if ($form_sesion->isValid()) {
                $authenticacion = new Authentication($auth, $this->getServiceLocator());
                $authenticacion->VerifyUser($formData);
                if ($authenticacion->isOk()) {
                    $status = 'ok';
                } else {
                    $messages = $authenticacion->getMessages();

                    if (!$messages == 'Su cuenta esta deshabilitada') {
                        $status = 'disable';
                    } else {
                        if ($messages[0] == 'activar-cuenta') {
                            $status = 'activar';
                        } else {
                            $status = 'error';
                        }
                    }
                }
            } else {
                $status = 'error';
                $array_response = $form_sesion->getMessages();
                $messages = $this->getValues($array_response);
            }
        }
        $result = array(
            'status' => $status,
            'messages' => $messages
        );
//        \Zend\Debug\Debug::dump($result); exit;
        $response = $this->getResponse();
        $response->getHeaders()->addHeaderLine('Content-Type', 'application/json');
        $response->setContent(json_encode($result));
        return $response;
//        if ($status=='ok'){
////            return $this->redirect()->toRoute('dashboard');
//
//        }else if($status=='activar') {
////           echo $result;
//            return $this->redirect()->toRoute('activar-cuenta');
////            return $response;
//        }else{
//            /*$this->flashMessenger()->addErrorMessage($result);
//            return $this->redirect()->toRoute('home');*/
//            return $response;
//        }

    }

    public function logoutAction()
    {
        $auth = $this->getAuthService();
        $auth->clearIdentity();
        $sessionManager = new SessionManager();
        $sessionManager->getStorage()->clear();
        return $this->redirect()->toRoute('home');
    }

    public function loggedAction()
    {
        $status = 'notlogged';
        $auth = $this->getAuthService();
        if ($auth->hasIdentity() ) {
            $status = 'logged';
        }

        $result = array(
            'status' => $status
        );

        $response = $this->getResponse();
        $response->getHeaders()->addHeaderLine('Content-Type', 'application/json');
        $response->setContent(json_encode($result));

        return $response;
    }

    public function getAuthService()
    {
        if (! $this->authservice) {
            $this->authservice = $this->getServiceLocator()->get('AuthService');
        }

        return $this->authservice;
    }

    public function getValues($array){

        $messages = array();

        if(array_key_exists("username", $array)){
            $array["username"] = "Correo inválido";
        }
        $this->getMessages($array, $messages);

        return $messages;
    }

    public function getMessages($array, &$array_response = null){
        $response = array();
        foreach($array as $key=>$value){
            if (is_array($value)){
                $this->getMessages($value, $array_response);
            }else{
                $response[] = $value;
            }
        }
        if(!empty($response)){
            foreach ($response as $key=>$value){
                $array_response[] = $value;
            }
        }
    }
}

