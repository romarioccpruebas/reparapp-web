<?php

namespace Application\Interfaces;

interface AdminInterface
{
    public function setService($name,$service);
    public function setForm($name,$form);
    public function setModel($name,$model);

}
