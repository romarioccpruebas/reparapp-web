<?php

namespace Core\Model;

class User
{
    protected $serviceLocator;
    private $dbadapter;
    private $userModel;

    public function __construct($servicelocator)
    {
        $this->serviceLocator = $servicelocator;
        $this->config         = $this->serviceLocator->get('config');
        $this->dbadapter      = $this->serviceLocator->get('DbAdapter');
        $this->userModel= $this->serviceLocator->get('UserModel');
    }

    public function getUsers(){
        $data = $this->userModel->getAllUsers();
        return $data;
    }

    public function getUserById($id){

    }

    public function getServiceLocator(){
        return $this->serviceLocator;
    }

}