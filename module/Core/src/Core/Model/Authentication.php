<?php

namespace Core\Model;

use Core\Model\User;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Session\Container;

class Authentication
{
    public $data_user;
    public $ok;
    public $messages;
    protected $authService;
    protected $serviceLocator;

    public function __construct($authService,ServiceLocatorInterface $servicelocator) {
        $this->data_user = null;
        $this->ok = false;
        $this->messages = array();
        $this->authService = $authService;
        $this->serviceLocator = $servicelocator;
    }

    public function VerifyUser($data){

        $auth = $this->getAuthService();
        $auth->getAdapter()
            ->setIdentity($data->email)
            ->setCredential($data->password);
        $result = $auth->authenticate();

        if ($result->isValid()) {
            $dbAdapter = $this->getServiceLocator()->get('Zend\Db\Adapter\Adapter');
            $data_user = $auth->getAdapter()->getResultRowObject(null,'password');
            $data_array = get_object_vars($data_user);
            $user = new User($this->getServiceLocator());
            $user->exchangeArray($data_array);

            if($user->enable==1){
                $auth->getStorage()->write(
                    $data_user
                );
                $this->ok=true;
            }else{
                if ($user->enable==2){
                    $auth->getStorage()->write(null);
                    $this->messages[] = "Su cuenta esta deshabilitada";
                }else{
                    $auth->getStorage()->write(null);
                    $this->messages[] = "activar-cuenta";
                    $auth->getStorage()->write(
                        $data_user
                    );
                }


            }
        }else{
            $dbAdapter = $this->getServiceLocator()->get('Zend\Db\Adapter\Adapter');
            $statement = $dbAdapter->query("SELECT count(*) as isUserEntel FROM users WHERE email LIKE '$data->email' 
             -- AND enable = 1 
-- AND (password IS NULL OR password = '')
");
            $user = $statement->execute()->current();
            if($user['isUserEntel']){
                $this->messages[] = "Credenciales incorrectas";
            }else{
                $this->messages[] = "Correo inválido";
            }
        }

    }

    public function VerifyEmail($data){
        $username   = $data['txtEmail'];
        $dbAdapter  = $this->getServiceLocator()->get('Zend\Db\Adapter\Adapter');
        $statement  = $dbAdapter->query("SELECT * FROM user WHERE email LIKE '$username'");
        $user_data  = $statement->execute()->current();
        if(!empty($user_data)){
            if ($user_data['enable']==0){
                $this->messages = "Correo registrado, falta activar la cuenta";
            }else{
                $this->messages = "El correo ingresado ya esta registrado";
            }

        }else{
            $this->messages = "El correo que ingresaste no está registrado en e-pass";
            $this->ok = true;
        }

    }

    public function isOk(){
        return $this->ok;
    }

    public function getMessages(){
        return $this->messages;
    }

    public function getAuthService(){
        return $this->authService;
    }

    public function getServiceLocator(){
        return $this->serviceLocator;
    }

}

