<?php
/**
 * Description of AclActionTable
 *
 * @author Carlos Ramos
 */
namespace Core\Model\Table;

use Reparapp\Db\Table\BaseAdapter;
use Zend\Db\Sql\Sql;

class UserTable extends BaseAdapter
{
    const TABLE_NAME = 'users';
    
    public function getDataUserbyId($id){
        $sql = new Sql($this->getAdapter());
        $select = $sql->select()
            ->from(array('up' => self::TABLE_NAME))
            ->columns(array('*'))
            ->where(array('up.id' => $id));

        $result = $this->fetchRow($select);
        return $result;
    }

    public function getAllUsers(){
        $sql = new Sql($this->getAdapter());
        $select = $sql->select()
            ->from(array('up' => self::TABLE_NAME))
            ->columns(array('full_name','email','DNI'));

        $result = $this->fetchAll($select);
        return $result;
    }
}