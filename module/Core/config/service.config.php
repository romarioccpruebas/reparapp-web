<?php

namespace Core;

use Zend\ServiceManager\ServiceLocatorInterface;
use Reparapp\Db\Table\AdapterDbServiceAwareInterface;
use Reparapp\MongoDB\Collection\AdapterCollectionServiceAwareInterface;
use Zend\Db\Adapter\Adapter as DbAdapter;
use Reparapp\MongoDB\Adapter\MongoDB as MongoAdapter;

$tablas = include __DIR__ . '/tablesmap_autoload.php';
$collections = include __DIR__ . '/tablesmap_collection_autoload.php';

return [
    'initializers' => [
        function ($instance, ServiceLocatorInterface $sm) {
            if ($instance instanceof AdapterDbServiceAwareInterface) {
                $instance->setAdapter($sm->get('DbAdapter'));
            }
            if ($instance instanceof AdapterCollectionServiceAwareInterface) {
                $instance->setAdapter($sm->get('MongoAdapter'));
            }
        },
    ],
    'factories' => array_merge([
        'DbAdapter' => function ($sm) {
            $config = $sm->get('config');
            $config = $config['db'];
            $dbAdapter = new DbAdapter($config);
            return $dbAdapter;
        },
        'MongoAdapter' => function($sm){
            $config = $sm->get('config');
            $config = $config['mongodb']['db'];
            return new MongoAdapter($config);
        }
    ],$tablas, $collections)
];
