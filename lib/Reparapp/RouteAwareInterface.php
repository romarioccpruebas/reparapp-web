<?php

namespace Reparapp;

interface RouteAwareInterface
{

    public function setRoute($route);

}
