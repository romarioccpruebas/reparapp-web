<?php

namespace Reparapp\Utils;

class FormatoString {

    protected static $_pluralVocal = 's';
    protected static $_pluralConsonante = 'es';
    protected static $_vocales = 'aeiou';
    protected static $dep = array();
    protected static $prov = array();
    protected static $dist = array();

    public static function setTextoFormatoUrl($string, $maxlen = 0) {
        $newStringTab = array();
        $string = strtolower(self::quitarDiacriticas($string));
        $stringTab = str_split($string);
        $numbers = array("0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "-");

        foreach ($stringTab as $letter) {
            if (in_array($letter, range("a", "z")) || in_array($letter, $numbers)) {
                $newStringTab[] = $letter;
            } elseif ($letter == " ") {
                $newStringTab[] = "-";
            }
        }

        if (count($newStringTab)) {
            $newString = implode($newStringTab);
            if ($maxlen > 0) {
                $newString = substr($newString, 0, $maxlen);
            }
            $newString = self::removerDuplicados('--', '-', $newString);
        } else {
            $newString = '';
        }

        return $newString;
    }

    public static function quitarDiacriticas($string, $permitidos = array()) {
        //cyrylic transcription
        $cyrylicFrom = array('А', 'Б', 'В', 'Г', 'Д', 'Е', 'Ё', 'Ж', 'З', 'И', 'Й',
            'К', 'Л', 'М', 'Н', 'О', 'П', 'Р', 'С', 'Т', 'У', 'Ф', 'Х', 'Ц', 'Ч',
            'Ш', 'Щ', 'Ъ', 'Ы', 'Ь', 'Э', 'Ю', 'Я', 'а', 'б', 'в', 'г', 'д', 'е',
            'ё', 'ж', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т',
            'у', 'ф', 'х', 'ц', 'ч', 'ш', 'щ', 'ъ', 'ы', 'ь', 'э', 'ю', 'я');
        $cyrylicTo = array('A', 'B', 'W', 'G', 'D', 'Ie', 'Io', 'Z', 'Z', 'I', 'J',
            'K', 'L', 'M', 'N', 'O', 'P', 'R', 'S', 'T', 'U', 'F', 'Ch', 'C', 'Tch',
            'Sh', 'Shtch', '', 'Y', '', 'E', 'Iu', 'Ia', 'a', 'b', 'w', 'g', 'd',
            'ie', 'io', 'z', 'z', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'r', 's',
            't', 'u', 'f', 'ch', 'c', 'tch', 'sh', 'shtch', '', 'y', '', 'e', 'iu',
            'ia');

        $from = array("Á", "À", "Â", "Ä", "Ă", "Ā", "Ã", "Å", "Ą", "Æ", "Ć", "Ċ",
            "Ĉ", "Č", "Ç", "Ď", "Đ", "Ð", "É", "È", "Ė", "Ê", "Ë", "Ě", "Ē", "Ę",
            "Ə", "Ġ", "Ĝ", "Ğ", "Ģ", "á", "à", "â", "ä", "ă", "ā", "ã", "å", "ą",
            "æ", "ć", "ċ", "ĉ", "č", "ç", "ď", "đ", "ð", "é", "è", "ė", "ê", "ë",
            "ě", "ē", "ę", "ə", "ġ", "ĝ", "ğ", "ģ", "Ĥ", "Ħ", "I", "Í", "Ì", "İ",
            "Î", "Ï", "Ī", "Į", "Ĳ", "Ĵ", "Ķ", "Ļ", "Ł", "Ń", "Ň", "Ñ", "Ņ", "Ó",
            "Ò", "Ô", "Ö", "Õ", "Ő", "Ø", "Ơ", "Œ", "ĥ", "ħ", "ı", "í", "ì", "i",
            "î", "ï", "ī", "į", "ĳ", "ĵ", "ķ", "ļ", "ł", "ń", "ň", "ñ", "ņ", "ó",
            "ò", "ô", "ö", "õ", "ő", "ø", "ơ", "œ", "Ŕ", "Ř", "Ś", "Ŝ", "Š", "Ş",
            "Ť", "Ţ", "Þ", "Ú", "Ù", "Û", "Ü", "Ŭ", "Ū", "Ů", "Ų", "Ű", "Ư", "Ŵ",
            "Ý", "Ŷ", "Ÿ", "Ź", "Ż", "Ž", "ŕ", "ř", "ś", "ŝ", "š", "ş", "ß", "ť",
            "ţ", "þ", "ú", "ù", "û", "ü", "ŭ", "ū", "ů", "ų", "ű", "ư", "ŵ", "ý",
            "ŷ", "ÿ", "ź", "ż", "ž");
        $to = array("A", "A", "A", "A", "A", "A", "A", "A", "A", "AE", "C", "C",
            "C", "C", "C", "D", "D", "D", "E", "E", "E", "E", "E", "E", "E", "E",
            "G", "G", "G", "G", "G", "a", "a", "a", "a", "a", "a", "a", "a", "a",
            "ae", "c", "c", "c", "c", "c", "d", "d", "d", "e", "e", "e", "e", "e",
            "e", "e", "e", "g", "g", "g", "g", "g", "H", "H", "I", "I", "I", "I",
            "I", "I", "I", "I", "IJ", "J", "K", "L", "L", "N", "N", "N", "N", "O",
            "O", "O", "O", "O", "O", "O", "O", "CE", "h", "h", "i", "i", "i", "i",
            "i", "i", "i", "i", "ij", "j", "k", "l", "l", "n", "n", "n", "n", "o",
            "o", "o", "o", "o", "o", "o", "o", "o", "R", "R", "S", "S", "S", "S",
            "T", "T", "T", "U", "U", "U", "U", "U", "U", "U", "U", "U", "U", "W",
            "Y", "Y", "Y", "Z", "Z", "Z", "r", "r", "s", "s", "s", "s", "B", "t",
            "t", "b", "u", "u", "u", "u", "u", "u", "u", "u", "u", "u", "w", "y",
            "y", "y", "z", "z", "z");

        $from = array_merge($from, $cyrylicFrom);
        $to = array_merge($to, $cyrylicTo);

        //permnitidos
        foreach ($permitidos as $permitido) {
            if (in_array($permitido, $from)) {
                $copia = array_flip($from);
                $indice = $copia[$permitido];
                unset($from[$indice]);
                unset($to[$indice]);
            }
        }
        $newstring = str_replace($from, $to, $string);

        return $newstring;
    }

    public static function removerDuplicados($sSearch, $sReplace, $sSubject) {
        $i = 0;
        do {
            $sSubject = str_replace($sSearch, $sReplace, $sSubject);
            $pos = strpos($sSubject, $sSearch);
            $i++;
            if ($i > 100) {
                die('removerDuplicados() loop error');
            }
        } while ($pos !== false);

        return $sSubject;
    }

    public static function plural($cadena, $cantidad = 0) {
        if ($cantidad != 1) {
            $caracter = substr($cadena, strlen($cadena) - 1, 1);
            if (strpos(self::$_vocales, $caracter) !== FALSE) {
                $cadena .= self::$_pluralVocal;
            } elseif ($caracter !== self::$_pluralVocal) {
                $cadena .= self::$_pluralConsonante;
            }
        }

        return (string) $cadena;
    }

    public static function capitalizeTexto($texto) {
        $articulos = array(
            "de", "al", "el", "la", "y", "un", "una", "o", "del", "los", "las", "en"
        );

        $from = array('Á', 'É', 'Í', 'Ó', 'Ú', 'Ñ');
        $to = array('á', 'é', 'í', 'ó', 'ú', 'ñ');

        $texto = str_replace($from, $to, $texto);

        $palabras = explode(' ', $texto);

        $nuevoNombre = "";

        // parseamos cada palabra
        foreach ($palabras as $elemento) {
            // si la palabra es un articulo
            if (in_array(trim(strtolower($elemento)), $articulos)) {
                // concatenamos seguido de un espacio
                $nuevoNombre .= strtolower($elemento) . " ";
            } else {
                // sino, es un nombre propio, por lo tanto aplicamos
                // las funciones y concatenamos seguido de un espacio
                $nuevoNombre .= ucfirst(strtolower($elemento)) . " ";
            }
        }

        return trim($nuevoNombre);
    }

    public static function cortarTexto($txt, $nr) {
        $tamano = $nr;
        $contador = 0;
        $texto = strip_tags($txt);
        if ($texto != "") { // Cortamos la cadena por los espacios
            //$arrayTexto = split(' ',$texto);
            if ($tamano >= strlen($texto)) {
                return $texto;
            } else {
                $arrayTexto = explode(' ', $texto);
                $texto = '';

                // Reconstruimos la cadena
                while ($tamano >= strlen($texto) + strlen(@$arrayTexto[$contador])) {
                    $texto .= ' ' . @$arrayTexto[$contador];
                    $contador++;
                }
                return $texto . "... ";
            }
        }
        else
            return "Sin descripción";
    }

    public static function palabrasEnie($texto) {
        $from = array(
            'brena', 'Brena', 'BRENA',
            'canete', 'Canete', 'CANETE',
            'maranon', 'Maranon', 'MARANON',
            'ferrenafe', 'Ferrenafe', 'FERRENAFE',
            'zana', 'Zana', 'ZANA',
            'encanada', 'Encanada', 'ENCANADA',
            'banos del inca', 'Banos del Inca', 'BANOS DEL INCA',
        );
        $to = array(
            'breña', 'Breña', 'BREÑA',
            'cañete', 'Cañete', 'CAÑETE',
            'marañon', 'Marañon', 'MARAÑON',
            'ferreñafe', 'Ferreñafe', 'FERREÑAFE',
            'zaña', 'Zaña', 'ZAÑA',
            'encañada', 'Encañada', 'ENCAÑADA',
            'baños del inca', 'Baños del Inca', 'BAÑOS DEL INCA',
        );
        $texto = str_replace($from, $to, $texto);
        return $texto;
    }

    public static function unicode_decode($texto) {
        $result = preg_replace_callback('/\\\\u([0-9a-fA-F]{4})/', function ($match) {
                    return mb_convert_encoding(pack('H*', $match[1]), 'UTF-8', 'UCS-2BE');
                }, $texto);
        return $result;
    }

    public static function unicode_encode($texto) {
        if ($texto != '') {
            $table = array(utf8_decode("ñ") => "\\u00F1", utf8_decode("Ñ") => "\\u00D1", utf8_decode("ç") => "\\u00C7", utf8_decode("$") => "\\u0024",
                utf8_decode("&") => "\\u0026", utf8_decode("á") => "\\u00E1", utf8_decode("à") => "\\u00E0", utf8_decode("ã") => "\\u00E3", utf8_decode("â") => "\\u00E2", utf8_decode("ä") => "\\u00E4", utf8_decode("Á") => "\\u00C1",
                utf8_decode("À") => "\\u00C0", utf8_decode("Ã") => "\\u00C3", utf8_decode("Â") => "\\u00C2", utf8_decode("Ä") => "\\u00C4", utf8_decode("é") => "\\u00E9", utf8_decode("è") => "\\u00E8", utf8_decode("ë") => "\\u00EB",
                utf8_decode("ê") => "\\u00EA", utf8_decode("É") => "\\u00C9", utf8_decode("È") => "\\u00C8", utf8_decode("Ë") => "\\u00CB", utf8_decode("Ê") => "\\u00CA", utf8_decode("í") => "\\u00ED", utf8_decode("ì") => "\\u00EC",
                utf8_decode("ï") => "\\u00EF", utf8_decode("î") => "\\u00EE", utf8_decode("Í") => "\\u00ED", utf8_decode("Ì") => "\\u00EC", utf8_decode("Ï") => "\\u00EF", utf8_decode("Î") => "\\u00EE", utf8_decode("ó") => "\\u00F3",
                utf8_decode("ò") => "\\u00F2", utf8_decode("ö") => "\\u00F6", utf8_decode("ô") => "\\u00F4", utf8_decode("õ") => "\\u00F5", utf8_decode("Ó") => "\\u00D3", utf8_decode("Ò") => "\\u00D2", utf8_decode("Ö") => "\\u00D6",
                utf8_decode("Ô") => "\\u00D4", utf8_decode("Õ") => "\\u00D5", utf8_decode("ú") => "\\u00FA", utf8_decode("ù") => "\\u00F9", utf8_decode("ü") => "\\u00FC", utf8_decode("û") => "\\u00FB", utf8_decode("Ú") => "\\u00DA",
                utf8_decode("Ù") => "\\u00D9", utf8_decode("Ü") => "\\u00DC", utf8_decode("Û") => "\\u00DB", utf8_decode("|") => "\\u007C", utf8_decode(".") => "\\u002E", utf8_decode(",") => "\\u002C");
            $texto = utf8_decode($texto);
            $caracteres = str_split($texto);
            $keys = array_keys($table);
            foreach ($caracteres as $caracter) {
                if (in_array($caracter, $keys)) {
                    $rpta[] = $table[$caracter];
                    continue;
                }
                $rpta[] = $caracter;
            }
            return implode($rpta);
        }
        return $texto;
    }

    public static function getExpReg($url) {
        $exp = "#^\/(?<transaction_type>alquiler|venta|compra|alquiler-compra-venta)-de-(?<sub_cat>[a-z-_]+)-en-(?<cadena>[a-zA-Z0-9_\-\s%]+)(\/)?(?<vista>listado|mapa)?$#";
        if (strpos($url, 'buscar') > 0) {
            $exp = "#^\/(?<transaction_type>alquiler|venta|compra|alquiler-compra-venta)-de-(?<sub_cat>[a-z-_]+)-en-(?<cadena>[a-zA-Z0-9_\-\s%]+)-buscar-(?<libre>[a-zA-Z0-9_\-\s%]+)(\/)?(?<vista>listado|mapa)?$#";
        }
        // if (strpos($url, 'ciudad') > 0 || strpos($url, 'campo') > 0) {
        //     $exp = "#^\/(?<transaction_type>alquiler|venta|compra|alquiler-compra-venta)-de-(?<sub_cat>[a-z-]+)-en-(?<cadena>[a-zA-Z0-9_\-\s%]+)--(?<localizado>ciudad|campo)(\/)?(?<vista>listado|mapa)?$#";
        //     if (strpos($url, 'buscar') > 0) {
        //         $exp = "#^\/(?<transaction_type>alquiler|venta|compra|alquiler-compra-venta)-de-(?<sub_cat>[a-z-]+)-en-(?<cadena>[a-zA-Z0-9_\-\s%]+)--(?<localizado>ciudad|campo)-buscar-(?<libre>[a-zA-Z0-9_\-\s%]+)(\/)?(?<vista>listado|mapa)?$#";
        //     }
        // } else if (strpos($url, 'playa') > 0) {
        //     $exp = "#^\/(?<transaction_type>alquiler|venta|compra|alquiler-compra-venta)-de-(?<sub_cat>[a-z-]+)-de-(?<localizado>playa)-en-(?<cadena>[a-zA-Z0-9_\-\s%]+)(\/)?(?<vista>listado|mapa)?$#";
        //     if (strpos($url, 'buscar') > 0) {
        //         $exp = "#^\/(?<transaction_type>alquiler|venta|compra|alquiler-compra-venta)-de-(?<sub_cat>[a-z-]+)-de-(?<localizado>playa)-en-(?<cadena>[a-zA-Z0-9_\-\s%]+)(\/)?(?<vista>listado|mapa)?$#";
        //     }
        // }

        return $exp;
    }

    public static function getPortal($domains)
    {
        $categoria=['portal'=>'todobusco'];
        $serverName=$_SERVER['SERVER_NAME'];
        foreach ($domains as $domain) {
            if($domain['servers']['apphost']['host']==$serverName){
                $categoria['portal']=($domain['servers']['apphost']['render']!='')?$domain['servers']['apphost']['render']:'todobusco';
            }
        }
        return $categoria;
    }

    public static function autocompleteFields($fields) {
        $rpta = array('transaction_type' => 'venta', 'sub_cat' => '*', 'portal' => 'todobusco', 'cadena' => 'nicaragua', 'vista' => 'galeria');
        if ($fields['transaction_type'] == 'proyectos-inmobiliarios') {
            $fields['transaction_type'] = 'proyectos';
        }
        $rpta = array_merge($rpta, $fields);
        return $rpta;
    }

    public static function kvsEplanningFicha($data, $isProy) {
        $rpta = array("Operacion" => "venta", "Inmueble" => "departamento", "Busqueda" => "Santiago de Surco, Lima, Lima", "Proyecto" => "no");
        $operaciones = array('todos', 'venta', 'alquiler', 'alquiler_turista');
        $Inmueble = array('todos', 'casas', 'departamentos', 'locales', 'oficinas', 'terrenos', 'otros');
        $rpta['Operacion'] = (!$isProy) ? $operaciones[(int) $data['tipoTransac']] : 'venta';
        $rpta['Inmueble'] = strtolower($data['inm']);
        $rpta['Busqueda'] = (!empty($data['urbanizacion'])) ? sprintf("%s,%s,%s,%s", strtolower($data['urbanizacion']), $data['alias-dist'], strtolower($data['prov']), $data['alias-depa']) : sprintf("%s,%s,%s", strtolower($data['alias-dist']), strtolower($data['prov']), $data['alias-depa']);
        $rpta['Proyecto'] = ($isProy) ? "si" : "no";
        $rpta['Id_anunciante'] = $data['idUser'];
        return $rpta;
    }

    public static function kvsEplanningBuscador($data,$params) {


        $rpta = array('Operacion' => "venta", 'Inmueble' => "departamento", 'Busqueda' => "nicaragua", 'Proyecto' => "no");
        $operaciones = array('*'=>'todos', 'venta'=>'venta', 'alquiler'=>'alquiler', 'alquiler-turista'=>'alquiler_turista','alquiler-venta'=>'todos');
        $Inmueble = array('*' => 'todos', 'casa' => 'casas', 'departamento' => 'departamentos', 'local' => 'locales', 'oficina' => 'oficinas', 'terreno' => 'terrenos');
        $idOp=(isset($params['estado']))?$params['estado']:((isset($datos->estado->id))?$datos->estado->id:'venta');

        $datos = json_decode($data);
        $isProy = ($datos->tipo_aviso == 2) ? true : false;
        foreach ($datos->tipo_inmueble as $inmuebleDat) {
            $inmuebles[] = (isset($Inmueble[$inmuebleDat->id])) ? $Inmueble[$inmuebleDat->id] : 'otros';
        }
        $rpta['Operacion'] = (!$isProy) ? $operaciones[$idOp] : 'venta';
        $rpta['Inmueble'] = implode('|', $inmuebles);
        $libre=isset($params['libre'])?$params['libre']:NULL;

        $datosUbigeo = $datos->ubigeo;
        $rpta['Busqueda'] = implode('|',self::getDataBusqueda($datosUbigeo,$libre));
        $rpta['Proyecto'] = ($isProy) ? "si" : "no";
        return $rpta;
    }

    protected static function getDataBusqueda($ubigeo,$libre) {
        $ordUbigeo = array('playa', 'urbanizacion', 'distrito', 'provincia', 'departamento');
        $departamentos = array();
        $provincias = array();
        $distritos = array();
        $busqueda = array();
        $default=is_null($libre)?array('nicaragua'):array($libre);
        foreach ($ordUbigeo as $keyUbigeo) {
            if (!empty($ubigeo->{$keyUbigeo})) {
                switch ($keyUbigeo) {
                    case 'playa':
                        foreach ($ubigeo->{$keyUbigeo} as $keyply => $valueply) {
                            $id = $valueply->id;
                            foreach ($valueply->ancestors as $keyAnc => $valueAnc) {
                                if ($valueAnc->type == 0) {
                                    $departamentos[] = $dep = $valueAnc->id;
                                } else if ($valueAnc->type == 1) {
                                    $provincias[] = $prov = $valueAnc->id;
                                } else {
                                    $distritos[] = $dist = $valueAnc->id;
                                }
                            }
                            $busqueda[] = sprintf("%s,%s,%s,%s", $id, $dist, $prov, $dep);
                        }
                        break;
                    case 'urbanizacion':
                        foreach ($ubigeo->{$keyUbigeo} as $keyUrb => $valueUrb) {
                            $id = $valueUrb->id;
                            foreach ($valueUrb->ancestors as $keyAnc => $valueAnc) {
                                if ($valueAnc->type == 0) {
                                    $departamentos[] = $dep = $valueAnc->id;
                                } else if ($valueAnc->type == 1) {
                                    $provincias[] = $prov = $valueAnc->id;
                                } else {
                                    $distritos[] = $dist = $valueAnc->id;
                                }
                            }
                            $busqueda[] = sprintf("%s,%s,%s,%s", $id, $dist, $prov, $dep);
                        }
                        break;
                    case 'distrito':
                        foreach ($ubigeo->{$keyUbigeo} as $key => $value) {
                            $id = $value->id;
                            if (!in_array($id, $distritos)) {
                                foreach ($value->ancestors as $keyAnc => $valueAnc) {
                                    if ($valueAnc->type == 0) {
                                        $departamentos[] = $dep = $valueAnc->id;
                                    } else {
                                        $provincias[] = $prov = $valueAnc->id;
                                    }
                                }
                                $busqueda[] = sprintf("%s,%s,%s", $id, $prov, $dep);
                            }
                        }
                        break;
                    case 'provincia':
                        $value = $ubigeo->{$keyUbigeo};
                        $id = $value->id;
                        if (!in_array($id, $provincias)) {
                            foreach ($value->ancestors as $keyAnc => $valueAnc) {
                                if ($valueAnc->type == 0) {
                                    $departamentos[] = $dep = $valueAnc->id;
                                }
                            }
                            $busqueda[] = sprintf("%s,%s", $id, $dep);
                        }
                        break;
                    case 'departamento':
                        $value = $ubigeo->{$keyUbigeo};
                        $id = $value->id;
                        if (!in_array($id, $departamentos)) {
                            $busqueda[] = $id;
                        }
                        break;

                    default:
                        $busqueda[] = 'nicaragua';
                        break;
                }
            }
        }
        $busqueda=(empty($busqueda))?$default:$busqueda;
        return $busqueda;
    }

    public static function formatEplannig($json){
        $buscar=array('"Operacion"','"Inmueble"','"Busqueda"','"Proyecto"');//,'"kVs"','"sec"','"ss"','"eIs"'
        $remplazar=array('Operacion','Inmueble','Busqueda','Proyecto');//,'kVs','sec','ss','eIs'

        foreach ($buscar as $key=>$value) {
            $pos=strrpos($json,$value);
            $json=  substr_replace($json,$remplazar[$key],$pos,strlen($value));
        }

        return $json;

    }

}
