<?php

namespace Reparapp;

interface ConfigAwareInterface
{

    public function setConfig($config);

}
