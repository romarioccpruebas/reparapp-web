<?php

namespace Reparapp\Db\Table;

use Zend\Db\Adapter\Adapter;

interface AdapterDbServiceAwareInterface {

    public function setAdapter(Adapter $adapter);
}
