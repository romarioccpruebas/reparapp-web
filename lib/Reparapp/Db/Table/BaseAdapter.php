<?php

namespace Reparapp\Db\Table;

use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Reparapp\Db\Table\AdapterDbServiceAwareInterface;
use Zend\Db\Metadata\Metadata;
use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\TableGateway\TableGateway;
/**
 * Base adapter for sql layer.
 * @package Util
 * @author Angel Ybarhue <angel.ybarhuen@Reparappandbricks.pe>
 * @version 1.0.0
 */
abstract class BaseAdapter extends TableGateway implements AdapterDbServiceAwareInterface {

    /**
     * @var Adapter
     */
//    protected $adapter;

    /**
     * @var Sql
     */
//    protected $sql;

    public function __construct($table) {
        $this->table = $table;
    }
    
    public function setAdapter(Adapter $adapter) {
        parent::__construct($this->table, $adapter);
    }

    public function fetch($select) {
        $statement = $this->sql->prepareStatementForSqlObject($select);
        $resultSet = new ResultSet();
        $result = $statement->execute();
        return $resultSet->initialize($result)->toArray();
    }

    public function fetchAll($select)
    {
        $sql = $this->sql;
        $selectString = $sql->prepareStatementForSqlObject($select);
        $result = $selectString->execute();
        $resultSet = new ResultSet();
        return $resultSet->initialize($result)->toArray();

    }

    public function fetchRow($select)
    {
        $sql = $this->sql;
        $selectString = $sql->prepareStatementForSqlObject($select);
        return $selectString->execute()->current();
    }

    public function fetchAssoc($select)
    {
        $sql = $this->sql;
        $selectString = $sql->prepareStatementForSqlObject($select);
        $result = $selectString->execute();
        $resultSet = new ResultSet();
        $stmt = $resultSet->initialize($result)->toArray();
        $data = array();
        foreach ($stmt as $row) {
            $tmp = array_values(array_slice($row, 0, 1));
            $data[$tmp[0]] = $row;
        }

        return $data;
    }

    public function fetchCol($select)
    {
        $sql = $this->sql;
        $selectString = $sql->prepareStatementForSqlObject($select);
        $result = $selectString->execute();
        $resultSet = new ResultSet();
        $stmt = $resultSet->initialize($result)->toArray();
        $data = array();
        foreach ($stmt as $row) {
            $tmp = array_values(array_slice($row, 0, 1));
            $data[] = $tmp[0];
        }

        return $data;
    }

    public function fetchPairs($select)
    {
        $sql = $this->sql;
        $selectString = $sql->prepareStatementForSqlObject($select);
        $result = $selectString->execute();
        $resultSet = new ResultSet();
        $stmt = $resultSet->initialize($result)->toArray();
        $data = array();
        foreach ($stmt as $row) {
            $tmp = array_values(array_slice($row, 0, 2));
            $data[$tmp[0]] = $tmp[1];
        }

        return $data;
    }

    public function fetchOne($select)
    {
        $sql = $this->sql;
        $selectString = $sql->prepareStatementForSqlObject($select);
        $stmt = $selectString->execute()->current();
        if (empty($stmt))
            return 0;
        $tmp = array_values(array_slice($stmt, 0, 1));
        $result = $tmp[0];

        return $result;
    }

    public function getServiceLocator()
    {
        return $this->serviceLocator;
    }

    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
    }

    public function getCols()
    {
        $metadata = new Metadata($this->getAdapter());
        
        return $metadata->getColumnNames($this->getTable());
    }

    public function _guardar($datos)
    {
        $id = 0;

        if (!empty($datos['id'])) {
            $id = (int) $datos['id'];
        }
        unset($datos['id']);

        $datos = array_intersect_key($datos, array_flip($this->getCols()));
        $filter = new \Zend\Filter\StripTags(
                array(
                    'allowTags' => array('a','br'),
                    'allowAttribs' => 'href'
                    )
                );
        foreach ($datos as $key => $valor) {
            if (!is_array($valor)&&!is_numeric($valor))
                $datos[$key] = $filter->filter($valor);
        }

        if ($id > 0) {
            $cantidad = $this->update($datos, 'id = ' . $id);
            $id = ($cantidad < 1) ? 0 : $id;
        } else {
            $this->insert($datos);
            $id = $this->lastInsertValue;
        }

        return $id;
    }

    public function query($select)
    {
        return $this->getAdapter()->query($select, Adapter::QUERY_MODE_EXECUTE);
    }

    public function _guardarGenerico($datos, $clave = 'id', $action = 0)
    {
        $id = 0;

        if (!empty($datos[$clave])) {
            $id = (int) $datos[$clave];
        }
        unset($datos[$clave]);

        $datos = array_intersect_key($datos, array_flip($this->getCols()));

        foreach ($datos as $key => $valor) {
            if (!is_numeric($valor)) {
                $datos[$key] = str_replace("'", '"', $valor);
            }
        }
        if ($action > 0) {
            $cantidad = $this->update($datos, "$clave = " . $id);
            $id = ($cantidad < 1) ? 0 : $id;
        } else {
            $datos[$clave] = $id;
            $this->insert($datos);
            $id = $this->lastInsertValue;
        }

        return $id;
    }
    
//    public function getAdapter(){
//        return $this->adapter;
//    }
    
}
