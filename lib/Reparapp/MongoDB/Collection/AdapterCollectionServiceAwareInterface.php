<?php

namespace Reparapp\MongoDB\Collection;
use Reparapp\MongoDB\Adapter\MongoDB as MongoAdapter;

interface AdapterCollectionServiceAwareInterface {
    
    public function setAdapter(MongoAdapter $adapter);    
    
}

