<?php

namespace Reparapp\MongoDB;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Reparapp\MongoDB\Adapter\MongoDB as ReparappMongoDB;

class AdapterServiceFactory implements FactoryInterface
{

    /**
     * Create db adapter service
     *
     * @param  ServiceLocatorInterface $serviceLocator
     * @return Adapter
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $config = $serviceLocator->get('Config');

        return new ReparappMongoDB($config['mongodb']['db']);
    }

}
