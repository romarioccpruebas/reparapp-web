<?php

namespace Reparapp\View;

use Zend\ServiceManager\ConfigInterface;
use Zend\ServiceManager\ServiceManager;

class HelperConfig implements ConfigInterface
{

    /**
     * @var array Pre-aliased view helpers
     */
    protected $invokables = array(
        'action' => 'Reparapp\View\Helper\Action',
        'uri' => 'Reparapp\View\Helper\Uri',
        'link' => 'Reparapp\View\Helper\Link',
        'formAttr' => 'Reparapp\Form\View\Helper\FormAttr',
        'input' => 'Reparapp\Form\View\Helper\Input',
        'label' => 'Reparapp\Form\View\Helper\Label',
        'widget' => 'Reparapp\View\Helper\Widget',
        'textDelay' => 'Reparapp\View\Helper\TextDelay',
        'hasModule' => 'Reparapp\View\Helper\HasModule',
        'thumb' => 'Reparapp\View\Helper\Thumb',
        'datetime' => 'Reparapp\View\Helper\Datetime',
        'googleAnalytics' => 'Reparapp\View\Helper\GoogleAnalytics',
        'flashMessenger' => 'Reparapp\View\Helper\FlashMessenger',
        'gravatarLink' => 'Reparapp\View\Helper\GravatarLink',
        'subText' => 'Reparapp\View\Helper\SubText',
        'callback' => 'Reparapp\View\Helper\Callback',
        'assets' => 'Reparapp\View\Helper\Assets',
        'jsAssets' => 'Reparapp\View\Helper\JsAssets',
        'cssAssets' => 'Reparapp\View\Helper\CssAssets',
    );

    /**
     * Configure the provided service manager instance with the configuration
     * in this class.
     *
     * In addition to using each of the internal properties to configure the
     * service manager, also adds an initializer to inject ServiceManagerAware
     * classes with the service manager.
     *
     * @param  ServiceManager $serviceManager
     * @return void
     */
    public function configureServiceManager(ServiceManager $serviceManager)
    {
        foreach ($this->invokables as $name => $service) {
            $serviceManager->setInvokableClass($name, $service);
        }
    }

}
