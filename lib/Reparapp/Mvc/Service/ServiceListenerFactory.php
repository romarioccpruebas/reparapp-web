<?php

namespace Reparapp\Mvc\Service;

class ServiceListenerFactory extends \Zend\Mvc\Service\ServiceListenerFactory
{

    public function __construct()
    {
        $this->defaultServiceConfig['factories']['ViewHelperManager'] = 'Reparapp\Mvc\Service\ViewHelperManagerFactory';
    }

}
