<?php

namespace ZfSimpleMigrations\Migrations;

use ZfSimpleMigrations\Library\AbstractMigration;
use Zend\Db\Metadata\MetadataInterface;

class Version20170503053948 extends AbstractMigration
{
    public static $description = "Migration description";

    public function up(MetadataInterface $schema)
    {
        $this->addSql('
            SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
            SET time_zone = "America/Lima";

            CREATE TABLE IF NOT EXISTS `diagnostics` (
              `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
              `user_id` int(11) NOT NULL,
              `district` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
              `date` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
              `time` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
              `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
              `cellphone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
              `photo_1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
              `photo_2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
              `created_at` timestamp NOT NULL DEFAULT current_timestamp,
              `updated_at` timestamp NOT NULL DEFAULT current_timestamp,
              PRIMARY KEY (`id`),
              KEY `diagnostics_user_id_foreign` (`user_id`)
            ) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=15 ;
            ');
    }

    public function down(MetadataInterface $schema)
    {
        //throw new \RuntimeException('No way to go down!');
        //$this->addSql(/*Sql instruction*/);
    }
}
