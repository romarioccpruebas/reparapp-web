<?php

namespace ZfSimpleMigrations\Migrations;

use ZfSimpleMigrations\Library\AbstractMigration;
use Zend\Db\Metadata\MetadataInterface;

class Version20170503054308 extends AbstractMigration
{
    public static $description = "Migration description";

    public function up(MetadataInterface $schema)
    {
        $this->addSql('
            CREATE TABLE IF NOT EXISTS `users` (
              `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
              `full_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
              `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
              `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
              `type` enum(\'repairman\',\'client\',\'admin\') COLLATE utf8_unicode_ci NOT NULL,
              `state` enum(\'available\',\'not_available\') COLLATE utf8_unicode_ci NOT NULL,
              `cellphone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
              `dni` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
              `social_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
              `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
              `latitude` double DEFAULT NULL,
              `longitude` double DEFAULT NULL,
              `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
              `created_at` timestamp NOT NULL DEFAULT current_timestamp,
              `updated_at` timestamp NOT NULL DEFAULT current_timestamp,
              `deleted_at` timestamp NULL DEFAULT NULL,
              PRIMARY KEY (`id`)
            ) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=20 ;
            ');
    }

    public function down(MetadataInterface $schema)
    {
        //throw new \RuntimeException('No way to go down!');
        //$this->addSql(/*Sql instruction*/);
    }
}
