<?php

namespace ZfSimpleMigrations\Migrations;

use ZfSimpleMigrations\Library\AbstractMigration;
use Zend\Db\Metadata\MetadataInterface;

class Version20170503054307 extends AbstractMigration
{
    public static $description = "Migration description";

    public function up(MetadataInterface $schema)
    {
        $this->addSql('
            CREATE TABLE IF NOT EXISTS `service_requests` (
              `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
              `service_id` int(10) unsigned NOT NULL,
              `user_id` int(10) unsigned NOT NULL,
              `amount` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
              `total_price` double NOT NULL,
              `district` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
              `date` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
              `time` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
              `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
              `cellphone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
              `created_at` timestamp NOT NULL DEFAULT current_timestamp,
              `updated_at` timestamp NOT NULL DEFAULT current_timestamp,
              PRIMARY KEY (`id`),
              KEY `service_requests_service_id_foreign` (`service_id`),
              KEY `service_requests_user_id_foreign` (`user_id`)
            ) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=20 ;
            ');
    }

    public function down(MetadataInterface $schema)
    {
        //throw new \RuntimeException('No way to go down!');
        //$this->addSql(/*Sql instruction*/);
    }
}
